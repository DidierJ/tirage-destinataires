#!/bin/bash
#-------------------------------------------------------------------------------
# PROGRAMME D'ATTRIBUTION ALEATOIRE DE BINOMES 
# AUX MEMBRES D'UN GROUPE
#-------------------------------------------------------------------------------
# Auteur : D. JACQUES
# Inspire d'un post issu de
# https://delightlylinux.wordpress.com/2012/08/16/how-to-shuffle-a-bash-array/
# pour le tri aleatoire des participants
#-------------------------------------------------------------------------------
# Entree : - fichier "participants.txt" a alimenter
# Sortie : - dossier "destinataires" contenant des fichiers textes aux noms des
#            participants, chacun contenant le nom de son binome
#-------------------------------------------------------------------------------
#-------------------------------------------------------------------------------
# Historique     | Date       | Auteur       | Description
#                 --------------------------------------------------------------
#                | 14/12/2017 | D.JACQUES    | Creation
#-------------------------------------------------------------------------------

old_IFS=$IFS
IFS=$'\n'
FICHIER_PARTICIPANTS="participants.txt"
DOSSIER_DESTINATAIRES="destinataires"
PARTICIPANTS=()
DESTINATAIRES=()

#
# RECUPERATION DE LA LISTE DES PARTICIPANTS
#

echo "LECTURE ET LISTING DU FICHIER DES PARTICIPANTS..."
for line in $(cat $FICHIER_PARTICIPANTS); do
	echo "- $line"
	PARTICIPANTS+=("$line")
done
echo "... [OK]"

#
# SOME FUNCTIONS
#

function checkArray
{
	for item in ${DESTINATAIRES[@]}; do
	[[ "$item" == "$1" ]] && return 0
	done
	return 1
}

function shuffleAndGenerateFiles()
{
	if [ ! -d $DOSSIER_DESTINATAIRES ]; then
		mkdir $DOSSIER_DESTINATAIRES
	fi
	echo "SUPPRESSION DES FICHIERS EXISTANTS..." >&2
	rm -Rf $DOSSIER_DESTINATAIRES/*.txt
	echo "... [OK]" >&2

	echo "TRI ALEATOIRE DES PARTICIPANTS..." >&2
	while [ "${#DESTINATAIRES[@]}" -ne "${#PARTICIPANTS[@]}" ]; do
		RAND=$[ $RANDOM % ${#PARTICIPANTS[@]} ]
		checkArray "${PARTICIPANTS[$RAND]}" || DESTINATAIRES=(${DESTINATAIRES[@]} "${PARTICIPANTS[$RAND]}")
	done
	echo "... [OK]" >&2
	
	echo "GENERATION DES FICHIERS DESTINATAIRES..." >&2
	# On part du tableau des destinataires et on le decale d'un element pour etre sur de ne jamais attribuer 
	# a un participant son propre nom
	for I in ${!DESTINATAIRES[@]}; do
		if [ $I == $((${#DESTINATAIRES[@]} - 1)) ]; then
			echo "${DESTINATAIRES[0]}" > $DOSSIER_DESTINATAIRES/${DESTINATAIRES[$I]}.txt
		else
			IDX=$(($I + 1))
			echo "${DESTINATAIRES[$IDX]}" > $DOSSIER_DESTINATAIRES/${DESTINATAIRES[$I]}.txt	
		fi
	done
	echo "... [OK]" >&2
	return 1
}

echo ""                               

shuffleAndGenerateFiles

echo "FICHIERS DESTINATAIRES A RECUPERER DANS LE DOSSIER [$DOSSIER_DESTINATAIRES]."

echo "... [FIN]"

IFS=$old_IFS

exit 0

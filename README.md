# TIRAGE ALEATOIRE DE DESTINATAIRES
## Description
Programme BSHELL permettant de créer aléatoirement des "appairages" à partir d'une liste de participants.
Chaque participant se voit attribuer un destinataire parmi la liste des participants.
Peut être utilisé dans le cadre d'une distribution de cadeaux, par exemple, où chaque personne doit remettre un cadeau à la personne qui lui a été désignée.

## Utilisation
Déposer les fichiers **_distrib.sh_** et **_participants.txt_** dans un dossier local, puis alimenter le fichier **_participants.txt_** (une ligne par participant, éviter les caractères spéciaux) puis en ligne de commande, se placer dans le dossier et lancer simplement :

```
sh distrib.sh
```
Le programme crée ou met a jour un dossier _destinataires/_ qui contient alors un fichier **_*.txt_** par participant, contenant pour chacun le nom de son destinataire.
Les participants ne peuvent pas se voir attribuer leur propre nom.
___
## Credits
Le tri aléatoire de tableau a été directement inspire par [ce post](https://delightlylinux.wordpress.com/2012/08/16/how-to-shuffle-a-bash-array/).
